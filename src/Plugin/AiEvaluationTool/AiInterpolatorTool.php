<?php

namespace Drupal\ai_interpolator\Plugin\AiEvaluationTool;

use Drupal\ai_evaluation\PluginInterface\AiToolPluginInterface;
use Drupal\ai_evaluation\Attribute\AiTool;
use Drupal\Core\StringTranslation\TranslatableMarkup;


#[AiTool(
  id: 'ai_interpolator_tool',
  label: new TranslatableMarkup('AI Interpolator'),
  description: new TranslatableMarkup('Use the AI Interpolator to compare workflows.'),
)]
class AiInterpolatorTool implements AiToolPluginInterface {

}
